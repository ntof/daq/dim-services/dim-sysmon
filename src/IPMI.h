/*
 * IPMI.h
 *
 *  Created on: Jul 9, 2015
 *      Author: pperonna
 */

#ifndef IPMI_H_
#define IPMI_H_

#include <string>

#include <ipmi_monitoring.h>
#include <ipmi_monitoring_bitmasks.h>
#include <pugixml.hpp>

namespace ntof {
namespace dim {
class DIMDataSet;
class DIMState;
} // namespace dim

namespace system {

class IPMI
{
public:
    IPMI();
    explicit IPMI(const pugi::xml_node &impiCfgNode);
    virtual ~IPMI();

    /**
     * Opens IPMI driver
     */
    void openIPMI();

    /**
     * Read IPMI and print results (DIMData) into the specified node
     * @param dataSet DIM data set to copy data
     * @param state DIM state object to publish errors
     * @return number of sensors found
     */
    int readIPMI(ntof::dim::DIMDataSet &dataSet,
                 ntof::dim::DIMState &state,
                 ntof::dim::DIMDataSet &summarySet);

private:
    std::string getSensorType(int sensor_type);

    ipmi_monitoring_ctx_t ctx;
    bool enabled_;
    struct ipmi_monitoring_ipmi_config ipmi_config;
};
} /* namespace system */
} /* namespace ntof */

#endif /* IPMI_H_ */
