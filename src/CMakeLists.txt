
include_directories(
    ${CMAKE_SOURCE_DIR}/src
    ${CMAKE_CURRENT_BINARY_DIR})
include_directories(SYSTEM
    ${DIM_INCLUDE_DIRS}
    ${NTOFUTILS_INCLUDE_DIRS}
    ${PUGI_INCLUDE_DIRS}
    ${FREEIPMI_INCLUDE_DIRS})

set(SRC
  DiskSpace.cpp DiskSpace.h
  IPMI.cpp IPMI.h
  SysInfo.cpp SysInfo.h)

add_library(SysMonLib STATIC ${SRC})
target_link_libraries(SysMonLib ntofutils ${DIM_LIBRARIES} ${Boost_LIBRARIES}
${PUGI_LIBRARIES} ${FREEIPMI_LIBRARIES})
add_dependencies(SysMonLib DIM ntofutils)

add_executable(dim-sysmon nTOF_SystemMonitor.cpp)
target_link_libraries(dim-sysmon SysMonLib)

install(TARGETS dim-sysmon
        RUNTIME DESTINATION "${CMAKE_INSTALL_BINDIR}" COMPONENT runtime)
