
#include "IPMI.h"

#include <iostream>

#include <DIMData.h>
#include <DIMDataSet.h>
#include <DIMState.h>
#include <DIMXMLService.h>
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

using ntof::dim::AddMode;

namespace ntof {
namespace system {

IPMI::IPMI() : ctx(NULL), enabled_(true) {}

IPMI::IPMI(const pugi::xml_node &impiCfgNode) : ctx(NULL), enabled_(true)
{
    if (impiCfgNode)
    {
        enabled_ = impiCfgNode.attribute("enabled").as_bool(true);
    }
    else
    {
        std::cerr << "Warning, no IPMI node found in configuration file"
                  << std::endl;
    }
}

IPMI::~IPMI()
{
    if (ctx)
        ipmi_monitoring_ctx_destroy(ctx);
}

void IPMI::openIPMI()
{
    if (!enabled_)
    {
        return;
    }
    ipmi_config.driver_type = IPMI_MONITORING_DRIVER_TYPE_KCS;
    ipmi_config.disable_auto_probe = 0;
    ipmi_config.driver_address = 0;
    ipmi_config.register_spacing = 0;
    ipmi_config.driver_device = NULL;

    /*
    ipmi_config.protocol_version = IPMI_MONITORING_PROTOCOL_VERSION_1_5;
    ipmi_config.k_g = NULL;
    ipmi_config.k_g_len = 0;
    ipmi_config.privilege_level = IPMI_MONITORING_PRIVILEGE_LEVEL_USER;
    ipmi_config.authentication_type = IPMI_MONITORING_AUTHENTICATION_TYPE_MD5;
    ipmi_config.cipher_suite_id = 0;
    */
    ipmi_config.session_timeout_len = 100;
    ipmi_config.retransmission_timeout_len = 100;

    ipmi_config.workaround_flags = 0;

    /* Initialization flags
     *
     * Most commonly bitwise OR IPMI_MONITORING_FLAGS_DEBUG and/or
     * IPMI_MONITORING_FLAGS_DEBUG_IPMI_PACKETS for extra debugging
     * information.
     */
    int errnum = 0;
    if (ipmi_monitoring_init(0, &errnum) < 0)
    {
        std::string err = "ipmi_monitoring_init: " +
            std::string(ipmi_monitoring_ctx_strerror(errnum));
        throw err;
    }
    if (!(ctx = ipmi_monitoring_ctx_create()))
    {
        throw "ipmi_monitoring_ctx_create: " + std::string(strerror(errno));
    }

    if (ipmi_monitoring_ctx_sdr_cache_directory(ctx, "/tmp") < 0)
    {
        std::string err = "ipmi_monitoring_ctx_sdr_cache_directory: " +
            std::string(ipmi_monitoring_ctx_errormsg(ctx));
        throw err;
    }

    /* Must call otherwise only default interpretations ever used */
    if (ipmi_monitoring_ctx_sensor_config_file(ctx, NULL) < 0)
    {
        std::string err = "ipmi_monitoring_ctx_sensor_config_file: " +
            std::string(ipmi_monitoring_ctx_errormsg(ctx));
        throw err;
    }
}

/**
 * Read IPMI and print results (DIMData) into the specified node
 * @param insertNode
 */
int IPMI::readIPMI(ntof::dim::DIMDataSet &dataSet,
                   ntof::dim::DIMState &state,
                   ntof::dim::DIMDataSet & /*summarySet*/)
{
    if (!enabled_)
    {
        return 0;
    }
    unsigned int sensor_reading_flags = 0;
    int sensor_count = 0;

    if (ctx == NULL)
    {
        return sensor_count;
    }

    sensor_reading_flags |=
        IPMI_MONITORING_SENSOR_READING_FLAGS_IGNORE_NON_INTERPRETABLE_SENSORS;
    sensor_reading_flags |=
        IPMI_MONITORING_SENSOR_READING_FLAGS_INTERPRET_OEM_DATA;

    if ((sensor_count = ipmi_monitoring_sensor_readings_by_record_id(
             ctx, NULL, &ipmi_config, sensor_reading_flags, NULL, 0, NULL,
             NULL)) < 0)
    {
        std::string err = "ipmi_monitoring_sensor_readings_by_record_id: " +
            std::string(ipmi_monitoring_ctx_errormsg(ctx));
        std::cerr << err << std::endl;
        return sensor_count;
    }

    for (int i = 0; i < sensor_count;
         ++i, ipmi_monitoring_sensor_iterator_next(ctx))
    {
        int record_id, sensor_number, sensor_type, sensor_state, sensor_units,
            sensor_bitmask_type, sensor_bitmask, sensor_reading_type,
            event_reading_type_code;
        char **sensor_bitmask_strings = NULL;

        if ((record_id = ipmi_monitoring_sensor_read_record_id(ctx)) < 0)
        {
            std::string err = "ipmi_monitoring_sensor_read_record_id: " +
                std::string(ipmi_monitoring_ctx_errormsg(ctx));
            std::cerr << err << std::endl;
            return sensor_count;
        }

        if ((sensor_number = ipmi_monitoring_sensor_read_sensor_number(ctx)) < 0)
        {
            std::string err = "ipmi_monitoring_sensor_read_sensor_number: " +
                std::string(ipmi_monitoring_ctx_errormsg(ctx));
            std::cerr << err << std::endl;
            return sensor_count;
        }

        if ((sensor_type = ipmi_monitoring_sensor_read_sensor_type(ctx)) < 0)
        {
            std::string err = "ipmi_monitoring_sensor_read_sensor_type: " +
                std::string(ipmi_monitoring_ctx_errormsg(ctx));
            std::cerr << err << std::endl;
            return sensor_count;
        }

        std::string sensorName = std::string(
            ipmi_monitoring_sensor_read_sensor_name(ctx));

        if (sensorName.empty())
            sensorName = "N/A";

        // replace(sensorName.begin(), sensorName.end(), ' ', '_');
        if ((sensorName[0] == '+') || (sensorName[0] == ' '))
        {
            sensorName.erase(0, 1);
        }

        if ((sensor_state = ipmi_monitoring_sensor_read_sensor_state(ctx)) < 0)
        {
            std::string err = "ipmi_monitoring_sensor_read_sensor_state: " +
                std::string(ipmi_monitoring_ctx_errormsg(ctx));
            std::cerr << err << std::endl;
        }

        if ((sensor_units = ipmi_monitoring_sensor_read_sensor_units(ctx)) < 0)
        {
            std::string err = "ipmi_monitoring_sensor_read_sensor_units: " +
                std::string(ipmi_monitoring_ctx_errormsg(ctx));
            std::cerr << err << std::endl;
            return sensor_count;
        }

        if ((sensor_bitmask_type =
                 ipmi_monitoring_sensor_read_sensor_bitmask_type(ctx)) < 0)
        {
            std::string err = "ipmi_monitoring_sensor_read_sensor_bitmask_type:"
                              " " +
                std::string(ipmi_monitoring_ctx_errormsg(ctx));
            std::cerr << err << std::endl;
            return sensor_count;
        }

        if ((sensor_bitmask = ipmi_monitoring_sensor_read_sensor_bitmask(ctx)) <
            0)
        {
            std::string err = "ipmi_monitoring_sensor_read_sensor_bitmask: " +
                std::string(ipmi_monitoring_ctx_errormsg(ctx));
            std::cerr << err << std::endl;
            return sensor_count;
        }

        if (!(sensor_bitmask_strings =
                  ipmi_monitoring_sensor_read_sensor_bitmask_strings(ctx)))
        {
            std::string err = "ipmi_monitoring_sensor_read_sensor_bitmask_"
                              "strings: " +
                std::string(ipmi_monitoring_ctx_errormsg(ctx));
            std::cerr << err << std::endl;
            return sensor_count;
        }

        if ((sensor_reading_type =
                 ipmi_monitoring_sensor_read_sensor_reading_type(ctx)) < 0)
        {
            std::string err = "ipmi_monitoring_sensor_read_sensor_reading_type:"
                              " " +
                std::string(ipmi_monitoring_ctx_errormsg(ctx));
            std::cerr << err << std::endl;
            return sensor_count;
        }

        void *sensor_reading = ipmi_monitoring_sensor_read_sensor_reading(ctx);

        if ((event_reading_type_code =
                 ipmi_monitoring_sensor_read_event_reading_type_code(ctx)) < 0)
        {
            std::string err = "ipmi_monitoring_sensor_read_event_reading_type_"
                              "code: " +
                std::string(ipmi_monitoring_ctx_errormsg(ctx));
            std::cerr << err << std::endl;
            return sensor_count;
        }

        std::string sensorType = getSensorType(sensor_type);

        if (sensor_state == IPMI_MONITORING_STATE_NOMINAL)
        {
            state.removeError(i);
            state.removeWarning(i);
        }
        else if (sensor_state == IPMI_MONITORING_STATE_WARNING)
        {
            std::string warnName = sensorName + " IPMI warning";
            state.addWarning(i, warnName);
        }
        else if (sensor_state == IPMI_MONITORING_STATE_CRITICAL)
        {
            std::string errName = sensorName + " IPMI critical";
            state.addError(i, errName);
        }

        if (sensor_reading)
        {
            std::string sensorUnit;
            if (sensor_units == IPMI_MONITORING_SENSOR_UNITS_CELSIUS)
                sensorUnit = "C";
            else if (sensor_units == IPMI_MONITORING_SENSOR_UNITS_FAHRENHEIT)
                sensorUnit = "F";
            else if (sensor_units == IPMI_MONITORING_SENSOR_UNITS_VOLTS)
                sensorUnit = "V";
            else if (sensor_units == IPMI_MONITORING_SENSOR_UNITS_AMPS)
                sensorUnit = "A";
            else if (sensor_units == IPMI_MONITORING_SENSOR_UNITS_RPM)
                sensorUnit = "RPM";
            else if (sensor_units == IPMI_MONITORING_SENSOR_UNITS_WATTS)
                sensorUnit = "W";
            else if (sensor_units == IPMI_MONITORING_SENSOR_UNITS_PERCENT)
                sensorUnit = "%";

            if (sensor_reading_type ==
                IPMI_MONITORING_SENSOR_READING_TYPE_UNSIGNED_INTEGER8_BOOL)
            {
                bool value = *((uint8_t *) sensor_reading) ? true : false;
                dataSet.addData(sensorName, sensorUnit, value,
                                AddMode::OVERRIDE, false);
            }
            else if (sensor_reading_type ==
                     IPMI_MONITORING_SENSOR_READING_TYPE_UNSIGNED_INTEGER32)
            {
                int32_t value = *((int32_t *) sensor_reading);
                dataSet.addData(sensorName, sensorUnit, value,
                                AddMode::OVERRIDE, false);
            }
            else if (sensor_reading_type ==
                     IPMI_MONITORING_SENSOR_READING_TYPE_DOUBLE)
            {
                double value = *((double *) sensor_reading);
                dataSet.addData(sensorName, sensorUnit, value,
                                AddMode::OVERRIDE, false);
            }
            else
            {
                dataSet.addData(sensorName, sensorUnit, "N/A",
                                AddMode::OVERRIDE, false);
            }
        }
    }
    return sensor_count;
}

std::string IPMI::getSensorType(int sensor_type)
{
    switch (sensor_type)
    {
    case IPMI_MONITORING_SENSOR_TYPE_RESERVED: return ("Reserved");
    case IPMI_MONITORING_SENSOR_TYPE_TEMPERATURE: return ("Temperature");
    case IPMI_MONITORING_SENSOR_TYPE_VOLTAGE: return ("Voltage");
    case IPMI_MONITORING_SENSOR_TYPE_CURRENT: return ("Current");
    case IPMI_MONITORING_SENSOR_TYPE_FAN: return ("Fan");
    case IPMI_MONITORING_SENSOR_TYPE_PHYSICAL_SECURITY:
        return ("Physical Security");
    case IPMI_MONITORING_SENSOR_TYPE_PLATFORM_SECURITY_VIOLATION_ATTEMPT:
        return ("Platform Security Violation Attempt");
    case IPMI_MONITORING_SENSOR_TYPE_PROCESSOR: return ("Processor");
    case IPMI_MONITORING_SENSOR_TYPE_POWER_SUPPLY: return ("Power Supply");
    case IPMI_MONITORING_SENSOR_TYPE_POWER_UNIT: return ("Power Unit");
    case IPMI_MONITORING_SENSOR_TYPE_COOLING_DEVICE: return ("Cooling Device");
    case IPMI_MONITORING_SENSOR_TYPE_OTHER_UNITS_BASED_SENSOR:
        return ("Other Units Based Sensor");
    case IPMI_MONITORING_SENSOR_TYPE_MEMORY: return ("Memory");
    case IPMI_MONITORING_SENSOR_TYPE_DRIVE_SLOT: return ("Drive Slot");
    case IPMI_MONITORING_SENSOR_TYPE_POST_MEMORY_RESIZE:
        return ("POST Memory Resize");
    case IPMI_MONITORING_SENSOR_TYPE_SYSTEM_FIRMWARE_PROGRESS:
        return ("System Firmware Progress");
    case IPMI_MONITORING_SENSOR_TYPE_EVENT_LOGGING_DISABLED:
        return ("Event Logging Disabled");
    case IPMI_MONITORING_SENSOR_TYPE_WATCHDOG1: return ("Watchdog 1");
    case IPMI_MONITORING_SENSOR_TYPE_SYSTEM_EVENT: return ("System Event");
    case IPMI_MONITORING_SENSOR_TYPE_CRITICAL_INTERRUPT:
        return ("Critical Interrupt");
    case IPMI_MONITORING_SENSOR_TYPE_BUTTON_SWITCH: return ("Button/Switch");
    case IPMI_MONITORING_SENSOR_TYPE_MODULE_BOARD: return ("Module/Board");
    case IPMI_MONITORING_SENSOR_TYPE_MICROCONTROLLER_COPROCESSOR:
        return ("Microcontroller/Coprocessor");
    case IPMI_MONITORING_SENSOR_TYPE_ADD_IN_CARD: return ("Add In Card");
    case IPMI_MONITORING_SENSOR_TYPE_CHASSIS: return ("Chassis");
    case IPMI_MONITORING_SENSOR_TYPE_CHIP_SET: return ("Chip Set");
    case IPMI_MONITORING_SENSOR_TYPE_OTHER_FRU: return ("Other Fru");
    case IPMI_MONITORING_SENSOR_TYPE_CABLE_INTERCONNECT:
        return ("Cable/Interconnect");
    case IPMI_MONITORING_SENSOR_TYPE_TERMINATOR: return ("Terminator");
    case IPMI_MONITORING_SENSOR_TYPE_SYSTEM_BOOT_INITIATED:
        return ("System Boot Initiated");
    case IPMI_MONITORING_SENSOR_TYPE_BOOT_ERROR: return ("Boot Error");
    case IPMI_MONITORING_SENSOR_TYPE_OS_BOOT: return ("OS Boot");
    case IPMI_MONITORING_SENSOR_TYPE_OS_CRITICAL_STOP:
        return ("OS Critical Stop");
    case IPMI_MONITORING_SENSOR_TYPE_SLOT_CONNECTOR: return ("Slot/Connector");
    case IPMI_MONITORING_SENSOR_TYPE_SYSTEM_ACPI_POWER_STATE:
        return ("System ACPI Power State");
    case IPMI_MONITORING_SENSOR_TYPE_WATCHDOG2: return ("Watchdog 2");
    case IPMI_MONITORING_SENSOR_TYPE_PLATFORM_ALERT: return ("Platform Alert");
    case IPMI_MONITORING_SENSOR_TYPE_ENTITY_PRESENCE:
        return ("Entity Presence");
    case IPMI_MONITORING_SENSOR_TYPE_MONITOR_ASIC_IC:
        return ("Monitor ASIC/IC");
    case IPMI_MONITORING_SENSOR_TYPE_LAN: return ("LAN");
    case IPMI_MONITORING_SENSOR_TYPE_MANAGEMENT_SUBSYSTEM_HEALTH:
        return ("Management Subsystem Health");
    case IPMI_MONITORING_SENSOR_TYPE_BATTERY: return ("Battery");
    case IPMI_MONITORING_SENSOR_TYPE_SESSION_AUDIT: return ("Session Audit");
    case IPMI_MONITORING_SENSOR_TYPE_VERSION_CHANGE: return ("Version Change");
    case IPMI_MONITORING_SENSOR_TYPE_FRU_STATE: return ("FRU State");
    }

    return ("Unrecognized");
}

} // namespace system
} /* namespace ntof */
