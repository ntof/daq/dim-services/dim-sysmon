/*
 * SysInfo.h
 *
 *  Created on: Oct 5, 2015
 *      Author: mdonze
 */

#ifndef SYSINFO_H_
#define SYSINFO_H_

#include <map>
#include <vector>

#include <pugixml.hpp>

namespace ntof {
namespace dim {
class DIMDataSet;
class DIMState;
} // namespace dim

namespace system {

class SysInfo
{
public:
    SysInfo();
    explicit SysInfo(const pugi::xml_node &systemCfgNode);
    virtual ~SysInfo();

    /**
     * Read sysinfo and print results (DIMData) into the specified node
     * @param dataSet DIM data set to copy data
     * @param state DIM state object to publish errors
     */
    void readSysInfo(ntof::dim::DIMDataSet &dataSet,
                     ntof::dim::DIMState &state,
                     ntof::dim::DIMDataSet &summarySet);

private:
    /**
     * Initialize CPU readout
     */
    void initialize();

    /**
     * Gets the fixed point load average
     * @param load
     * @return
     */
    double getLoadAverage(unsigned long load);

    /**
     * Parse /proc/stat fields
     * @param fp
     * @param fields
     * @return
     */
    int readProcStatfields(FILE *fp, unsigned long long int *fields);

    /**
     * Read memory info and print results (DIMData) into the specified node
     * @param dataSet DIM data set to copy data
     * @param state DIM state object to publish errors
     */
    void readMemoryInfo(ntof::dim::DIMDataSet &dataSet,
                        ntof::dim::DIMState &state,
                        ntof::dim::DIMDataSet &summarySet);

    /**
     * Read memory info and print results (DIMData) into the specified node
     * @param dataSet DIM data set to copy data
     * @param state DIM state object to publish errors
     */
    void readLoadAverageInfo(ntof::dim::DIMDataSet &dataSet,
                             ntof::dim::DIMState &state,
                             ntof::dim::DIMDataSet &summarySet);

    /**
     * Read CPU info and print results (DIMData) into the specified node
     * @param dataSet DIM data set to copy data
     * @param state DIM state object to publish errors
     */
    void readCPUInfo(ntof::dim::DIMDataSet &dataSet,
                     ntof::dim::DIMState &state,
                     ntof::dim::DIMDataSet &summarySet);

    /**
     * Gets memory data from values
     * @param name
     * @param memValues
     * @return
     */
    uint64_t getMemoryData(const std::string &name,
                           std::map<std::string, uint64_t> &memValues);

    FILE *fpProcStat; //!< /proc/stat file pointer
    int cpus;         //!< Number of CPU
    std::vector<unsigned long long int> total_tick;
    std::vector<unsigned long long int> total_tick_old;
    std::vector<unsigned long long int> idle;
    std::vector<unsigned long long int> idle_old;
    std::vector<unsigned long long int> del_total_tick;
    std::vector<unsigned long long int> del_idle;

    double ramWarning;  //!<< RAM percentage warning threshold
    double ramError;    //!<< RAM percentage error threshold
    double loadWarning; //!<< Load average warning threshold
    double loadError;   //!<< Load average error threshold
};

} /* namespace system */
} /* namespace ntof */

#endif /* SYSINFO_H_ */
