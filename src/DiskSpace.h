/*
 * DiskSpace.h
 *
 *  Created on: Oct 1, 2015
 *      Author: mdonze
 */

#ifndef DISKSPACE_H_
#define DISKSPACE_H_

#include <set>
#include <string>

#include <pugixml.hpp>

namespace ntof {
namespace dim {
class DIMDataSet;
class DIMState;
} // namespace dim

namespace system {

class DiskSpace
{
public:
    DiskSpace();
    DiskSpace(double warnPC, double errorPC);
    explicit DiskSpace(const pugi::xml_node &diskCfgNode);
    virtual ~DiskSpace();

    /**
     * Read disk usage and print results (DIMData) into the specified node
     * @param dataSet DIM data set to copy data
     * @param state DIM state object to publish errors
     * @param summarySet DIM dataset to put summary items
     * @return number of disks found
     */
    int readDiskUsage(ntof::dim::DIMDataSet &dataSet,
                      ntof::dim::DIMState &state,
                      ntof::dim::DIMDataSet &summarySet);

private:
    double pcUsedWarning;     //!< Percentage of used disk to build a warning
    double pcUsedError;       //!< Percentage of used disk to build an error
    std::string partFavorite; //!< Favorite partition to monitor
    std::set<std::string> ignore; //!< Set of disks to be excluded

    /**
     * Gets mount point for given directory
     * @param dir
     * @return
     */
    std::string getMountPoint(const std::string &dir);
};

} /* namespace system */
} /* namespace ntof */

#endif /* DISKSPACE_H_ */
