/*
 * nTOF_SystemMonitor.cpp
 *
 *  Created on: Oct 1, 2015
 *      Author: mdonze
 */

#include <iostream>
#include <sstream>

#include <ConfigMisc.h>
#include <DIMDataSet.h>
#include <DIMState.h>
#include <DIMXMLService.h>
#include <pugixml.hpp>
#include <sys/time.h>
#include <unistd.h>

#include "DiskSpace.h"
#include "IPMI.h"
#include "SysInfo.h"

#include <dis.hxx>

std::string getHostName()
{
    // Gets the machine hostname
    std::string hostName;
    char hostname_cstr[128];
    hostname_cstr[0] = 0;
    gethostname(hostname_cstr, 128);
    hostName = hostname_cstr;
    size_t dotPos = hostName.find_first_of('.');
    if (dotPos != std::string::npos)
    {
        hostName = hostName.substr(0, dotPos);
    }
    return hostName;
}

/**
 * Main application entry point
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char **argv)
{
    try
    {
        ntof::utils::ConfigMisc conf;
        DimServer::setDnsNode(conf.getDimDns().c_str());
    }
    catch (...)
    {
        DimServer::setDnsNode("pcgw25.cern.ch");
    }
    std::string hostName = getHostName();
    std::string cfgFile = "/etc/ntof/sysmon.xml";
    if (argc > 1)
    {
        cfgFile = argv[1];
    }
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file(cfgFile.c_str());
    if (!result)
    {
        std::cerr << "A problem occurred during the opening of the "
                     "configuration file : "
                  << cfgFile << std::endl;
        std::cerr << "Default configuration will be used..." << std::endl;
    }
    const pugi::xml_node &root = doc.first_child();

    // Build IPMI object and connect to it
    ntof::system::IPMI ipmi(root.child("ipmi"));
    try
    {
        ipmi.openIPMI();
    }
    catch (...)
    {
        std::cerr << "Unable to open IPMI device, IPMI will not be acquired..."
                  << std::endl;
    }

    // Build disk space object
    ntof::system::DiskSpace ds(root.child("disks"));

    // Build system info object
    ntof::system::SysInfo sysInfo(root.child("system"));

    // Build DIM objects
    ntof::dim::DIMState state(hostName + "/SystemState");
    state.addStateValue(0, "UNDEFINED");
    state.addStateValue(1, "OK");
    state.setValue(1);
    state.setAutoRefresh(false);

    ntof::dim::DIMDataSet svc(hostName + "/System");
    ntof::dim::DIMDataSet svcSum(hostName + "/SystemSummary");
    std::string srvName = hostName + "_sys";
    DimServer::start(srvName.c_str());
    for (;;)
    {
        ds.readDiskUsage(svc, state, svcSum);
        sysInfo.readSysInfo(svc, state, svcSum);
        ipmi.readIPMI(svc, state, svcSum);
        svc.updateData();
        svcSum.updateData();
        state.refresh();
        sleep(10);
    }
    return 0;
}
