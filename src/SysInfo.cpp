/*
 * SysInfo.cpp
 *
 *  Created on: Oct 5, 2015
 *      Author: mdonze
 */

#include "SysInfo.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include <DIMData.h>
#include <DIMDataSet.h>
#include <DIMState.h>
#include <DIMXMLService.h>
#include <stdio.h>
#include <sys/sysinfo.h>
#include <unistd.h>

using ntof::dim::AddMode;

namespace ntof {
namespace system {

SysInfo::SysInfo(const pugi::xml_node &systemCfgNode) :
    fpProcStat(NULL),
    cpus(0),
    ramWarning(75.0),
    ramError(90.0),
    loadWarning(0.8),
    loadError(1.0)
{
    initialize();
    // Load configuration
    if (systemCfgNode)
    {
        const pugi::xml_node &ramNode = systemCfgNode.child("RAM");
        if (ramNode)
        {
            const pugi::xml_node &threshold = ramNode.child("threshold");
            if (threshold)
            {
                ramWarning = threshold.attribute("warning").as_double(75.0);
                ramError = threshold.attribute("error").as_double(90.0);
            }
            else
            {
                std::cerr << "Warning, no threshold node found in RAM system "
                             "configuration node"
                          << std::endl;
            }
        }
        else
        {
            std::cerr
                << "Warning, no RAM node found in system configuration node"
                << std::endl;
        }

        const pugi::xml_node &loadNode = systemCfgNode.child("load");
        if (loadNode)
        {
            const pugi::xml_node &threshold = loadNode.child("threshold");
            if (threshold)
            {
                loadWarning = threshold.attribute("warning").as_double(75.0);
                loadError = threshold.attribute("error").as_double(90.0);
            }
            else
            {
                std::cerr << "Warning, no threshold node found in load system "
                             "configuration node"
                          << std::endl;
            }
        }
        else
        {
            std::cerr
                << "Warning, no load node found in system configuration node"
                << std::endl;
        }
    }
    else
    {
        std::cerr << "Warning, no system node found in configuration file"
                  << std::endl;
    }
}

SysInfo::SysInfo() :
    fpProcStat(NULL),
    cpus(0),
    ramWarning(75.0),
    ramError(90.0),
    loadWarning(0.8),
    loadError(1.0)
{
    initialize();
}

SysInfo::~SysInfo()
{
    if (fpProcStat != NULL)
    {
        fclose(fpProcStat);
    }
}

/**
 * Initialize CPU readout
 */
void SysInfo::initialize()
{
    fpProcStat = fopen("/proc/stat", "r");
    if (fpProcStat == NULL)
    {
        perror("Error");
    }
    unsigned long long int fields[10];

    // Gets number of CPU
    while (readProcStatfields(fpProcStat, fields) != -1)
    {
        cpus++;
    }
    total_tick.reserve(cpus);
    total_tick_old.reserve(cpus);
    idle.reserve(cpus);
    idle_old.reserve(cpus);
    del_total_tick.reserve(cpus);
    del_idle.reserve(cpus);

    fseek(fpProcStat, 0, SEEK_SET);
    fflush(fpProcStat);
    // Gets number of CPU
    int cpu = 0;
    while (readProcStatfields(fpProcStat, fields) != -1)
    {
        total_tick.push_back(0);
        total_tick_old.push_back(0);
        idle.push_back(0);
        idle_old.push_back(0);
        del_total_tick.push_back(0);
        del_idle.push_back(0);

        for (int i = 0; i < 4; i++)
        {
            total_tick[cpu] += fields[i];
        }
        idle[cpu] = fields[3]; /* idle ticks index */
        cpu++;
    }
}

/**
 * Read sysinfo and print results (DIMData) into the specified node
 * @param dataSet DIM data set to copy data
 * @param state DIM state object to publish errors
 * @return number of disks found
 */
void SysInfo::readSysInfo(ntof::dim::DIMDataSet &dataSet,
                          ntof::dim::DIMState &state,
                          ntof::dim::DIMDataSet &summarySet)
{
    readMemoryInfo(dataSet, state, summarySet);
    readLoadAverageInfo(dataSet, state, summarySet);
    readCPUInfo(dataSet, state, summarySet);
}

uint64_t SysInfo::getMemoryData(const std::string &name,
                                std::map<std::string, uint64_t> &memValues)
{
    std::map<std::string, uint64_t>::iterator it = memValues.find(name);
    if (it != memValues.end())
    {
        uint64_t ret = it->second; // Should be in kB
        return ret * 1024;
    }
    else
    {
        return 0;
    }
}

/**
 * Read sysinfo and print results (DIMData) into the specified node
 * @param dataSet DIM data set to copy data
 * @param state DIM state object to publish errors
 * @return number of disks found
 */
void SysInfo::readMemoryInfo(ntof::dim::DIMDataSet &dataSet,
                             ntof::dim::DIMState &state,
                             ntof::dim::DIMDataSet &summarySet)
{
    try
    {
        std::map<std::string, uint64_t> memValues;
        std::ifstream ifS;
        ifS.open("/proc/meminfo");
        std::string line;
        while (std::getline(ifS, line))
        {
            std::istringstream stream(line);
            std::string tag;
            uint64_t value = 0;
            stream >> tag >> value;
            if (!tag.empty())
            {
                tag = tag.substr(0, tag.length() - 1);
                memValues[tag] = value;
            }
        }
        ifS.close();
        uint64_t memTotal = getMemoryData("MemTotal", memValues);
        uint64_t memFree = getMemoryData("MemAvailable", memValues);
        dataSet.addData("RAM total", "B", (int64_t) (memTotal),
                        AddMode::OVERRIDE, false);
        dataSet.addData("RAM free", "B", (int64_t) (memFree), AddMode::OVERRIDE,
                        false);

        double pcFree = (((double) memFree) / ((double) memTotal)) * 100.0;
        dataSet.addData("RAM free%", "%", pcFree, AddMode::OVERRIDE, false);
        dataSet.addData("RAM cached", "B",
                        (int64_t) (getMemoryData("Cached", memValues)),
                        AddMode::OVERRIDE, false);
        dataSet.addData("RAM buffers", "B",
                        (int64_t) (getMemoryData("Buffers", memValues)),
                        AddMode::OVERRIDE, false);
        dataSet.addData("Swap total", "B",
                        (int64_t) (getMemoryData("SwapTotal", memValues)),
                        AddMode::OVERRIDE, false);
        dataSet.addData("Swap free", "B",
                        (int64_t) (getMemoryData("SwapFree", memValues)),
                        AddMode::OVERRIDE, false);
        summarySet.addData("RAM used%", "%", (100.0 - pcFree),
                           AddMode::OVERRIDE, false);

        double pcUsed = 100.0 - pcFree;
        if (pcUsed > ramError)
        {
            std::ostringstream oss;
            oss << pcUsed << "% of RAM used";
            state.addError(2001, oss.str());
            state.removeWarning(2001);
        }
        else if (pcUsed > ramWarning)
        {
            std::ostringstream oss;
            oss << pcUsed << "% of RAM used";
            state.addWarning(2001, oss.str());
            state.removeError(2001);
        }
        else
        {
            state.removeError(2001);
            state.removeWarning(2001);
        }
        state.removeError(10001);
    }
    catch (...)
    {
        state.addError(10001, "Unable to open /proc/meminfo");
    }
}

/**
 * Read sysinfo and print results (DIMData) into the specified node
 * @param dataSet DIM data set to copy data
 * @param state DIM state object to publish errors
 * @return number of disks found
 */
void SysInfo::readLoadAverageInfo(ntof::dim::DIMDataSet &dataSet,
                                  ntof::dim::DIMState &state,
                                  ntof::dim::DIMDataSet & /*summarySet*/)
{
    struct sysinfo info;
    sysinfo(&info);
    dataSet.addData("System uptime", "s", info.uptime, AddMode::OVERRIDE, false);
    dataSet.addData("Load average 1m", "", getLoadAverage(info.loads[0]),
                    AddMode::OVERRIDE, false);
    double loadAvg5m = getLoadAverage(info.loads[1]);
    dataSet.addData("Load average 5m", "", loadAvg5m, AddMode::OVERRIDE, false);
    dataSet.addData("Load average 15m", "", getLoadAverage(info.loads[2]),
                    AddMode::OVERRIDE, false);
    dataSet.addData("Running processes", "", info.procs, AddMode::OVERRIDE,
                    false);

    if (loadAvg5m > loadError)
    {
        std::ostringstream oss;
        oss << "system load exceeded (" << loadAvg5m << ")";
        state.addError(2002, oss.str());
        state.removeWarning(2002);
    }
    else if (loadAvg5m > loadWarning)
    {
        std::ostringstream oss;
        oss << "system load exceeded (" << loadAvg5m << ")";
        state.addWarning(2002, oss.str());
        state.removeError(2002);
    }
    else
    {
        state.removeError(2002);
        state.removeWarning(2002);
    }
}

/**
 * Read sysinfo and print results (DIMData) into the specified node
 * @param dataSet DIM data set to copy data
 * @param state DIM state object to publish errors
 * @return number of disks found
 */
void SysInfo::readCPUInfo(ntof::dim::DIMDataSet &dataSet,
                          ntof::dim::DIMState & /*state*/,
                          ntof::dim::DIMDataSet &summarySet)
{
    unsigned long long int fields[10];
    fseek(fpProcStat, 0, SEEK_SET);
    fflush(fpProcStat);
    for (int count = 0; count < cpus; count++)
    {
        total_tick_old[count] = total_tick[count];
        idle_old[count] = idle[count];

        if (!readProcStatfields(fpProcStat, fields))
        {
            std::cerr << "readProcStatfields failed!" << std::endl;
            return;
        }

        total_tick[count] = 0;
        for (int i = 0; i < 4; i++)
        {
            total_tick[count] += fields[i];
        }
        idle[count] = fields[3];

        del_total_tick[count] = total_tick[count] - total_tick_old[count];
        del_idle[count] = idle[count] - idle_old[count];

        double percent_usage = ((del_total_tick[count] - del_idle[count]) /
                                (double) del_total_tick[count]) *
            100;
        if (count == 0)
        {
            dataSet.addData("Total CPU Usage", "%", percent_usage,
                            AddMode::OVERRIDE, false);
            summarySet.addData("Total CPU Usage", "%", percent_usage,
                               AddMode::OVERRIDE, false);
        }
        else
        {
            std::ostringstream oss;
            oss << "CPU #" << (count - 1) << " usage";
            dataSet.addData(oss.str(), "%", percent_usage, AddMode::OVERRIDE,
                            false);
        }
    }
}

/**
 * Gets the fixed point load average
 * @param load
 * @return
 */
double SysInfo::getLoadAverage(unsigned long load)
{
    return ((double) load) / ((double) (1 << SI_LOAD_SHIFT));
}

/**
 * Parse /proc/stat fields
 * @param fp
 * @param fields
 * @return
 */
int SysInfo::readProcStatfields(FILE *fp, unsigned long long int *fields)
{
    int retval;
    char buffer[1024];

    for (int i = 0; i < 10; ++i)
    {
        fields[i] = 0;
    }

    if (!fgets(buffer, 1024, fp))
    {
        perror("Error");
    }
    /* line starts with c and a string. This is to handle cpu, cpu[0-9]+ */
    retval = sscanf(buffer,
                    "cp%*s %llu %llu %llu %llu %llu %llu %llu %llu %llu %llu",
                    &fields[0], &fields[1], &fields[2], &fields[3], &fields[4],
                    &fields[5], &fields[6], &fields[7], &fields[8], &fields[9]);
    if (retval == 0)
    {
        return -1;
    }
    if (retval < 4) /* Atleast 4 fields is to be read */
    {
        std::cout << retval << " ==> buffer : " << buffer << std::endl;
        fprintf(stderr, "Error reading /proc/stat cpu field\n");
        return 0;
    }
    return 1;
}

} /* namespace system */
} /* namespace ntof */
