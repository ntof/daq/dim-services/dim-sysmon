/*
 * DiskSpace.cpp
 *
 *  Created on: Oct 1, 2015
 *      Author: mdonze
 */

#include "DiskSpace.h"

#include <algorithm>
#include <cctype>
#include <iostream>
#include <string>
#include <vector>

#include <DIMData.h>
#include <DIMDataSet.h>
#include <DIMState.h>
#include <DIMXMLService.h>
#include <libgen.h>
#include <mntent.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <unistd.h>

using ntof::dim::AddMode;

namespace ntof {
namespace system {

struct mountdetail
{
    std::string mnt_fsname; /* Device or server for filesystem.  */
    std::string mnt_dir;    /* Directory mounted on.  */
    std::string mnt_type;   /* Type of filesystem: ufs, nfs, etc.  */
    std::string mnt_opts;   /* Comma-separated options for fs.  */
    int mnt_freq;           /* Dump frequency (in days).  */
    int mnt_passno;         /* Pass number for `fsck'.  */

    bool operator<(const mountdetail &str) const
    {
        return (mnt_fsname < str.mnt_fsname);
    }
};

DiskSpace::DiskSpace(double warnPC, double errorPC) :
    pcUsedWarning(warnPC), pcUsedError(errorPC)
{}

DiskSpace::DiskSpace() : pcUsedWarning(80.0), pcUsedError(90.0) {}

DiskSpace::DiskSpace(const pugi::xml_node &diskCfgNode) :
    pcUsedWarning(80.0), pcUsedError(90.0)
{
    if (diskCfgNode)
    {
        const pugi::xml_node &threshold = diskCfgNode.child("threshold");
        if (threshold)
        {
            pcUsedWarning = threshold.attribute("warning").as_double(80.0);
            pcUsedError = threshold.attribute("error").as_double(90.0);
        }
        else
        {
            std::cerr
                << "Warning, no threshold node found in disk configuration node"
                << std::endl;
        }

        const pugi::xml_node &exclude = diskCfgNode.child("exclude");
        for (pugi::xml_node part = exclude.child("partition"); part;
             part = part.next_sibling("partition"))
        {
            const pugi::xml_attribute &nameAttr = part.attribute("name");
            if (nameAttr)
            {
                ignore.insert(nameAttr.value());
                std::cout << "Will ignore " << nameAttr.value() << std::endl;
            }
        }

        partFavorite =
            diskCfgNode.child("favorite").attribute("directory").as_string("");
        if (!partFavorite.empty())
        {
            partFavorite = getMountPoint(partFavorite);
            std::cout << "Favorite directory mounted on " << partFavorite
                      << std::endl;
        }
    }
    else
    {
        std::cerr << "Warning, no disk node found in configuration file"
                  << std::endl;
    }
}

DiskSpace::~DiskSpace() {}

/**
 * Gets mount point for given directory
 * @param dir
 * @return
 */
std::string DiskSpace::getMountPoint(const std::string &dir)
{
    std::string ret;
    struct stat64 last_stat;
    __dev_t lastDev = 0;
    __ino_t lastIno = 0;

    char *cwd = getcwd(NULL, 0);

    if (stat64(dir.c_str(), &last_stat) < 0)
    {
        goto end;
    }

    // Check if is a directory
    if (S_ISDIR(last_stat.st_mode))
    {
        // We only need to chdir
        if (chdir(dir.c_str()) < 0)
        {
            goto end;
        }
    }
    else
    {
        const char *dirS = dirname((char *) &dir[0]);
        // Move to parent directory
        if (chdir(dirS) < 0)
        {
            goto end;
        }
        // Gets entry stat
        if (stat64(dirS, &last_stat) < 0)
        {
            goto end;
        }
    }

    lastDev = last_stat.st_dev;
    lastIno = last_stat.st_ino;
    while (true)
    {
        struct stat st;
        if (stat("..", &st) < 0)
        {
            goto end;
        }
        if (st.st_dev != lastDev || st.st_ino == lastIno)
        {
            /* cwd is the mount point.  */
            char *cwdM = getcwd(NULL, 0);
            ret = cwdM;
            free(cwdM);
            break;
        }
        if (chdir("..") < 0)
        {
            goto end;
        }
        lastDev = st.st_dev;
        lastIno = st.st_ino;
    }

end:
    // Restore current working directory
    chdir(cwd);
    free(cwd);
    return ret;
}

/**
 * Read disk usage and print results (DIMData) into the specified node
 * @param dataSet DIM data set to copy data
 * @param state DIM state object to publish errors
 * @return number of disks found
 */
int DiskSpace::readDiskUsage(ntof::dim::DIMDataSet &dataSet,
                             ntof::dim::DIMState &state,
                             ntof::dim::DIMDataSet &summarySet)
{
    struct mntent *ent;
    FILE *aFile;

    aFile = setmntent("/proc/mounts", "r");
    if (aFile == NULL)
    {
        perror("setmntent");
        return 0;
    }

    std::vector<struct mountdetail> mounts;

    while (NULL != (ent = getmntent(aFile)))
    {
        std::string name = ent->mnt_fsname;
        if (name.find("/dev/") == 0)
        {
            struct mountdetail d;
            d.mnt_dir = ent->mnt_dir;
            d.mnt_freq = ent->mnt_freq;
            d.mnt_fsname = ent->mnt_fsname;
            d.mnt_opts = ent->mnt_opts;
            d.mnt_passno = ent->mnt_passno;
            d.mnt_type = ent->mnt_type;
            mounts.push_back(d);
        }
    }
    endmntent(aFile);
    std::sort(mounts.begin(), mounts.end());

    int i = 0;
    for (std::vector<struct mountdetail>::iterator it = mounts.begin();
         it != mounts.end(); ++it)
    {
        ++i;
        struct statfs64 stat;
        if (statfs64(it->mnt_dir.c_str(), &stat) == -1)
        {
            perror("statfs64");
        }
        else
        {
            int64_t fsSize = stat.f_bsize * stat.f_blocks;
            int64_t fsFree = stat.f_bsize * stat.f_bfree;
            int64_t fsAvail = stat.f_bsize * stat.f_bavail;
            int64_t fsUsed = fsSize - fsFree;
            double pcUsed = ((double) (fsSize - fsAvail) / (double) fsSize) *
                100.0;
            std::string sizeSvc = it->mnt_dir + " size";
            dataSet.addData(sizeSvc, "B", fsSize, AddMode::OVERRIDE, false);
            std::string availSvc = it->mnt_dir + " available";
            dataSet.addData(availSvc, "B", fsAvail, AddMode::OVERRIDE, false);
            std::string usedSvc = it->mnt_dir + " used";
            dataSet.addData(usedSvc, "B", fsUsed, AddMode::OVERRIDE, false);
            std::string usedPCSvc = it->mnt_dir + " used%";
            dataSet.addData(usedPCSvc, "%", pcUsed, AddMode::OVERRIDE, false);
            if (!partFavorite.empty() && (it->mnt_dir == partFavorite))
            {
                // Favorite partition
                summarySet.addData("Disk used%", "%", pcUsed, AddMode::OVERRIDE,
                                   false);
            }

            if (ignore.find(it->mnt_fsname) == ignore.end())
            {
                if (pcUsed > pcUsedError)
                {
                    std::ostringstream oss;
                    oss << pcUsed << "% of disk used on " << it->mnt_dir;
                    state.addError(1000 + i, oss.str());
                    state.removeWarning(1000 + i);
                }
                else if (pcUsed > pcUsedWarning)
                {
                    std::ostringstream oss;
                    oss << pcUsed << "% of disk used on " << it->mnt_dir;
                    state.addWarning(1000 + i, oss.str());
                    state.removeError(1000 + i);
                }
                else
                {
                    state.removeError(1000 + i);
                    state.removeWarning(1000 + i);
                }
            }
        }
    }
    return mounts.size();
}

} /* namespace system */
} /* namespace ntof */
